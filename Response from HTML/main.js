const http = require('http'),
  fs = require('fs');

http.createServer((req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/html; charset=utf-8'
  });

  fs.readFile('./index.html', (err, data) => {
    res.end(data);
  })
}).listen(5500, () => console.log('running'));