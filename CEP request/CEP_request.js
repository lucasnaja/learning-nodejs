const http = require('http');

const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  res.writeHead(200);

  const CEP = '12519370';

  const GET = http.request({
    method: 'GET',
    hostname: 'viacep.com.br',
    path: `/ws/${CEP}/json`,
    port: 80
  }, resp => {
    let response = '';

    resp.on('data', chunk => {
      response += chunk
    })

    resp.on('end', () => {
      let str = '';

      if (!response.includes('"erro": true') && !response.startsWith('<')) {
        for (let obj in JSON.parse(response))
          str += `${obj}: ${JSON.parse(response)[obj]}\n`

        res.end(str)
      } else res.end('CEP inválido!')
    })

    resp.on('error', err => { throw err })
  })

  GET.end()
})

server.listen(3000)